import platform
import os

sistema = platform.system()
print("Tu plataforma es: " + str(sistema) + ".")
pausa = input("Introduce una tecla para continuar")

if platform.system() == 'Linux':
    verif_os = True
    print("Tu plataforma es: Linux")
else:
    verif_os = False

if verif_os == True:
    os.system('clear')
else:
    os.system('cls')

while True:
    
    print("Calculadora simple en Python")
    print(" ")
    print("Este programa te permitira hacer una operacion de: ")
    print("1. Adicion.")
    print("2. Sustraccion.")
    print("3. Division.")
    print("4. Multiplicacion.")
    print("5. Modulo de una division.")
    print("6. Salir")
    print(" ")
    
    verif = 's'
    opcion = input("Introduce tu opcion: ")
    
    if opcion == '1':
        while verif == 's':
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
            print("Has elegido: Adicion.")
            num1 = float(input("Introduce un numero: "))
            num2 = float(input("Introduce otro numero: "))
            resul = str(num1 + num2)
            print("El resultado es " + resul + ".")
            verif = input("Quieres hacer otra suma? s/n: ")
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
        
    elif opcion == '2':
        while verif == 's':
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
            print("Has elegido: Sustraccion.")
            num1 = float(input("Introduce un numero: "))
            num2 = float(input("Introduce otro numero: "))
            resul = str(num1 - num2)
            print("El resultado es " + resul +".")
            verif = input("Quieres hacer otra sustraccion? s/n: ")
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
    
    elif opcion == '3':
        while verif == 's':
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
            print("Has elegido: Division.")
            num1 = float(input("Introduce un numero: "))
            num2 = float(input("Introduce otro numero: "))
            resul = str(num1 / num2)
            print("El resultado es " + resul + ".")
            verif = input("Quieres hacer otra division? s/n: ")
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
        
    elif opcion == '4':
        while verif == 's':
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
            print("Has elegido: Multiplicacion.")
            num1 = float(input("Introduce un numero: "))
            num2 = float(input("Introduce otro numero: "))
            resul = str(num1 * num2)
            print("El resultado es " + resul + ".")
            verif = input("Quieres hacer otra multipicacion? s/n: ")
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
        
    elif opcion == '5':
        while verif == 's':
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
            print("Has elegido: Modulo de una division.")
            num1 = float(input("Introduce un numero: "))
            num2 = float(input("Introduce otro numero: "))
            resul = str(int(num1 % num2))
            print("El resultado es " + resul + ".")
            verif = input("Quieres calcular otro modulo? s/n: ")
            if verif_os == True:
                os.system('clear')
            else:
                os.system('cls')
        
    elif opcion == '6':
        if verif_os == True:
            os.system('clear')
        else:
            os.system('cls')
        break
    
    elif opcion == "\x3a\x76":
        if verif_os == True:
            os.system('clear')
        else:
            os.system('cls')
        
        print(" ")
        print(" ")
        
        print("\x20\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x2e\x20\x20\x20\x20\x0a\x20\x20\x20\x20\x20\x20\x2e\x2d\x2e\x20\x20\x20\x2e\x2d\x2e\x20\x20\x20\x20\x20\x2e\x2d\x2d\x2e\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x20\x20\x20\x20\x7c\x20\x4f\x4f\x7c\x20\x7c\x20\x4f\x4f\x7c\x20\x20\x20\x2f\x20\x5f\x2e\x2d\x27\x20\x2e\x2d\x2e\x20\x20\x20\x2e\x2d\x2e\x20\x20\x2e\x2d\x2e\x20\x20\x20\x2e\x27\x27\x2e\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x20\x20\x20\x20\x7c\x20\x20\x20\x7c\x20\x7c\x20\x20\x20\x7c\x20\x20\x20\x5c\x20\x20\x27\x2d\x2e\x20\x27\x2d\x27\x20\x20\x20\x27\x2d\x27\x20\x20\x27\x2d\x27\x20\x20\x20\x27\x2e\x2e\x27\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x20\x20\x20\x20\x27\x5e\x5e\x5e\x27\x20\x27\x5e\x5e\x5e\x27\x20\x20\x20\x20\x27\x2d\x2d\x27\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x2e\x20\x20\x2e\x2d\x2e\x20\x20\x2e\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x2e\x20\x20\x2e\x2d\x2e\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x7c\x20\x20\x20\x7c\x20\x7c\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x20\x27\x2d\x27\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x7c\x20\x20\x20\x7c\x20\x7c\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x27\x3a\x2d\x3a\x27\x20\x7c\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x20\x2e\x2d\x2e\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x6c\x34\x32\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x20\x27\x2d\x27\x20\x20\x7c\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x7c\x20\x20\x27\x2d\x27\x20\x20\x7c\x20\x20\x20\x20\x0a\x20\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x27\x20\x20\x20\x20\x20\x20\x20\x27\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x3d\x27\x20\x20\x20\x20\x20\x20\x20\x7c")
        
        print(" ")
        print(u'\u266B' + " What ya say? What ya say? What ya say? What? " + u'\u266B')
        pausa = input("Presiona ENTER para continuar.")
        if verif_os == True:
            os.system('clear')
        else:
            os.system('cls')
    
    else:
        print("Opcion no valida, intenta de nuevo.")
        pausa = input("Presiona ENTER para continuar.")
        if verif_os == True:
            os.system('clear')
        else:
            os.system('cls')
